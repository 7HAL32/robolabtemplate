# Robolab Template

## How to use:
1. Clone this repository (only **one** member of the team)

     `git clone https://bitbucket.org/7HAL32/robolabtemplate`

2. Set the up the remote upstream to your group repository  

    `git remote set-URL origin https://bitbucket.org/serobolabws1516/group-<XXX>`

3. Initial push 

    `git push origin master`

4. Set default upstream 

    `git branch --set-upstream master origin/master`

5. The second member of the team can now clone your repository.